# Formularios

Edita el fichero `index.html` para que tenga muestre un formulario que tenga los sigiuentes campos:

-Hecho **Nombre**, con un control de tipo texto que sea obligatorio cubrirlo y con autofoco.
-Hecho **Correo electrónico**, con un control de tipo email que sea obligatorio.
- Hecho **URL**, con un control de tipo url que cuando esté vacío muestre dentro la ayuda "Escribe la URL de tu página web personal" usando la propiedad _placeholder_.
-Hecho **Fecha**, con un control de tipo date.
- Hecho **Tiempo**, con un control de tipo time.
- Hecho  **Fecha y hora**, con un control de tipo datetime.
- Hecho **Mes**, con un control de tipo month.
- Hecho **Semana**, con un control de tipo week.
- Hecho **Número**, con un control de tipo number que limite la entrada a un valor entre -10 y 10.
- Hecho**Teléfono**, con un control de tipo tel que tenga un valor inicial y que no se pueda editar.
- **Término de búsqueda**, con un control de tipo search y que este deshabiltado.
-Hecho  **Color favorito**, con un control de tipo color.
- hecho Hecho Un botón de envío.

Además, tienes que tener en cuenta los siguientes requisitos:

- Todos los campos anteriores deben tener su label asociado mediante la propiedad _for_.
- El formulario debe estar estructurado usando las etiquetas fieldset (con sus legend)
- El método de envío del formulario debe ser GET.
- El destino del envío del formulario debe ser "".

El HTML resultante debe ser validado por el [validador de HTML de la W3](https://validator.w3.org/#validate_by_input) y no dar ningún error.
